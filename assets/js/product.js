$(document).ready(function () {
	var token = $("body").data("token");

	getDataProduk();
	function getDataProduk() {
		var form = new FormData();
		var settings = {
			async: true,
			crossDomain: true,
			url: "" + rest.url + "/api/product",
			method: "GET",
			headers: {
				authorization: token,
			},
		};
		// Tampilkan Product
		$.ajax(settings).done(function (response) {
			$(".content").remove();
			// console.log(response.data);
			$.each(response.data, function (key, value) {
				// console.log(value.name_product);
				var content =
					"<div class='col-md-2 content'>" +
					" <div class='card'>" +
					"<img class='card-img-top' src='" +
					rest.url +
					"/assets/img/" +
					value.image +
					"' >" +
					"<div class='card-body'>" +
					"<h4 class='card-title'>" +
					value.name_product +
					"</h4>" +
					"<p class='card-text'>" +
					value.price +
					"</p>" +
					"<button type='button' class='btn btn-warning btn-sm d-flex' data-toggle='modal' data-target='#modelEdit' id='btn-edit' data-id='" +
					value.id +
					"'>" +
					"Edit" +
					"</button>" +
					" <a class='btn btn-danger btn-sm' id='btn-hapus' data-id='" +
					value.id +
					"'>Hapus</a>" +
					"</div>" +
					"</div>" +
					"</div>";
				// console.log(content);
				$("#list_produk").append(content);
			});
		});
	}
	$("#myform").validate({
		rules: {
			name_product: {
				required: true,
			},
			price: {
				required: true,
			},
			image: {
				required: true,
			},
		},
		//added ajax
		submitHandler: function (myform) {
			Swal.fire({
				title: "Are you sure?",
				text: "the product will be added!",
				icon: "info",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "yes, add product",
			}).then((result) => {
				if (result.value) {
					var file = $("#image")[0].files[0];
					var form = new FormData();
					form.append("name_product", $("#name_product").val());
					form.append("image", file);
					form.append("price", $("#price").val());

					var settings = {
						url: "" + rest.url + "/api/product/add",
						method: "POST",
						timeout: 0,
						headers: {
							Authorization: token,
						},
						processData: false,
						mimeType: "multipart/form-data",
						contentType: false,
						data: form,
					};
					$.ajax(settings).done(function (response) {
						Swal.fire({
							position: "top-end",
							icon: "success",
							title: "Product Added",
							showConfirmButton: false,
							timer: 1500,
						});
						$("#myform")[0].reset();
						getDataProduk();
					});
				}
			});
		},
	}),
		//  ajax Edit
		$(document).on("click", "#btn-edit", function () {
			var form = new FormData();
			form.append("id", $(this).attr("data-id"));
			// var id = $(this).attr('data-id');
			// console.log(id);
			// return;
			var settings = {
				async: true,
				crossDomain: true,
				url: "" + rest.url + "/api/product?id=" + $(this).attr("data-id"),
				method: "GET",
				headers: {
					authorization: token,
				},
			};

			$.ajax(settings).done(function (response) {
				var dataP = response.data;
				// console.log(data.name_product);
				// return;
				$('#formEdit input[name =  "name_product"]').val(dataP.name_product);
				$('#formEdit input[name =  "price"]').val(dataP.price);
				$('#formEdit input[name =  "id"]').val(dataP.id);
			});
		}),
		$("#formEdit").validate({
			rules: {
				name_product: {
					required: true,
				},
				price: {
					required: true,
				},
				image: {
					required: true,
				},
			},
			submitHandler: function () {
				Swal.fire({
					title: "Are you sure?",
					text: "the product will be updated!",
					icon: "info",
					showCancelButton: true,
					confirmButtonColor: "#3085d6",
					cancelButtonColor: "#d33",
					confirmButtonText: "yes, update product",
				}).then((result) => {
					if (result.value) {
						var file = $("#Eimage")[0].files[0];
						var form = new FormData();
						form.append("_method", "PUT");
						form.append("id", $("#Eid").val());
						form.append("name_product", $("#Ename_product").val());
						form.append("image", file);
						form.append("price", $("#Eprice").val());

						var settings = {
							async: true,
							crossDomain: true,
							url: "" + rest.url + "/api/product/update",
							method: "POST",
							headers: {
								authorization: token,
							},
							processData: false,
							contentType: false,
							mimeType: "multipart/form-data",
							data: form,
						};

						$.ajax(settings).done(function (response) {
							Swal.fire({
								position: "top-end",
								icon: "success",
								title: "Product Added",
								showConfirmButton: false,
								timer: 1500,
							});
							$("#formEdit")[0].reset();
							getDataProduk();
						});
					}
				});
			},
		}),
		// ajax btn hapus
		$(document).on("click", "#btn-hapus", function (e) {
			// console.log($(this).attr('data-id'));
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: "btn btn-success",
					cancelButton: "btn btn-danger",
				},
				buttonsStyling: false,
			});

			swalWithBootstrapButtons
				.fire({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					icon: "warning",
					showCancelButton: true,
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "No, cancel!",
					reverseButtons: true,
				})
				.then((result) => {
					if (result.value) {
						var settings = {
							async: true,
							crossDomain: true,
							url:
								"" +
								rest.url +
								"/api/product/" +
								$(this).attr("data-id") +
								"/delete",
							method: "DELETE",
							headers: {
								authorization: token,
							},
						};
						$.ajax(settings).done(function (response) {
							getDataProduk();
							swalWithBootstrapButtons.fire(
								"Deleted!",
								"Your file has been deleted.",
								"success"
							);
						});
					} else if (
						/* Read more about handling dismissals below */
						result.dismiss === Swal.DismissReason.cancel
					) {
						swalWithBootstrapButtons.fire(
							"Cancelled",
							"Your imaginary file is safe :)",
							"error"
						);
					}
					// location.reload()
				});
		});
});
