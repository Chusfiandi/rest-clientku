$(document).ready(function () {
	var token = $("body").data("token");
	getDataCustomer();
	function getDataCustomer() {
		var form = new FormData();
		var settings = {
			async: true,
			crossDomain: true,
			url: "http://localhost/rest-apiku/api/customer",
			method: "GET",
			timeout: 0,
			headers: {
				Authorization: token,
			},
		};

		$.ajax(settings).done(function (response) {
			$(".content").remove();
			$.each(response.data, function (key, value) {
				var content =
					"<tr class='content'>" +
					'<td class="pt-3-half" contenteditable="true">' +
					(key + 1) +
					"</td>" +
					'<td class="pt-3-half" contenteditable="true">' +
					value.id +
					"</td>" +
					'<td class="pt-3-half" contenteditable="true">' +
					value.name +
					"</td>" +
					'<td class="pt-3-half" contenteditable="true">' +
					value.username +
					"</td>" +
					'<td class="pt-3-half" contenteditable="true">' +
					value.created_at +
					"</td>" +
					"<td>" +
					'<span class="table-remove">' +
					'<button type="button" class="btn btn-danger btn-rounded btn-sm my-0" id="btn-hapus" data-id="' +
					value.id +
					'">' +
					"Remove" +
					"</button>" +
					"</span>" +
					"</td>" +
					"</tr>";
				$("#list-cus").append(content);
			});
		});
	}
	// ajax btn hapus
	$(document).on("click", "#btn-hapus", function (e) {
		// console.log($(this).attr('data-id'));
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: "btn btn-success",
				cancelButton: "btn btn-danger",
			},
			buttonsStyling: false,
		});

		swalWithBootstrapButtons
			.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel!",
				reverseButtons: true,
			})
			.then((result) => {
				if (result.value) {
					var settings = {
						async: true,
						crossDomain: true,
						url:
							"" +
							rest.url +
							"/api/customer/" +
							$(this).attr("data-id") +
							"/delete",
						method: "DELETE",
						headers: {
							authorization: token,
						},
					};
					$.ajax(settings).done(function (response) {
						swalWithBootstrapButtons.fire(
							"Deleted!",
							"Your gift has been deleted.",
							"success"
						);
						getDataCustomer();
					});
				} else if (
					/* Read more about handling dismissals below */
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						"Cancelled",
						"Your imaginary gift is safe :)",
						"error"
					);
				}
				// location.reload()
			});
	});
	$("#myform").validate({
		rules: {
			name: {
				required: true,
			},
			username: {
				required: true,
				unique: true,
			},
			password: {
				required: true,
			},
		},
		//added ajax
		submitHandler: function (myform) {
			Swal.fire({
				title: "Are you sure?",
				text: "the customer will be added!",
				icon: "info",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "yes, add customer",
			}).then((result) => {
				if (result.value) {
					var form = new FormData();
					form.append("name", $("#name").val());
					form.append("username", $("#name").val());
					form.append("password", $("#password").val());

					var settings = {
						url: "" + rest.url + "/api/customer/add",
						method: "POST",
						timeout: 0,
						headers: {
							Authorization: token,
						},
						processData: false,
						mimeType: "multipart/form-data",
						contentType: false,
						data: form,
					};
					$.ajax(settings).done(function (response) {
						Swal.fire({
							position: "top-end",
							icon: "success",
							title: "customer Added",
							showConfirmButton: false,
							timer: 1500,
						});
						$("#myform")[0].reset();
						getDataCustomer();
					});
				}
			});
		},
	});
});
