  <!--Navbar-->
  <nav class="mb-1 navbar navbar-expand-lg navbar-light blue">

      <!-- <div class="container"> -->

      <a class="navbar-brand" href="#">
          <img src="<?= base_url('assets/img/mjp.png') ?>" height="30" alt="mdb logo">
      </a>

      <!-- Collapse button -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
          aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="basicExampleNav">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                  <a class="nav-link" href="#">Home
                      <span class="sr-only">(current)</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pages/product') ?>">Product</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pages/hadiah') ?>">Hadiah</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('pages/customer') ?>">Customer</a>
              </li>
          </ul>
          <!-- Dropdown -->
          <ul class="navbar-nav ml-auto nav-flex-icons pl-5">
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
                      aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-user"></i><b><?= $this->session->userdata('username') ?></b>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-default"
                      aria-labelledby="navbarDropdownMenuLink-333">
                      <a class="dropdown-item" href="#">Change Password</a>
                      <a class="dropdown-item" href="<?= base_url('auth/logout') ?>">Logout</a>
                  </div>
              </li>

          </ul>

      </div>
      <!-- 
      </div> -->

  </nav>
  <!--/.Navbar-->