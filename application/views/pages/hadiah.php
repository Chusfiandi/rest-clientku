<section id="form_tambah_data" class="">
    <!-- Button trigger modal -->
    <div class="container">
        <button type="button" class="btn btn-primary btn-sml" data-toggle="modal" data-target="#modelId">
            Tambah Hadiah
        </button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="myform">
                            <h4 class="card-title text-center">Form tambah Hadiah</h4>
                            <p class="text-center">Silahkan isi form</p>
                            <br>
                            <div class="form-group">
                                <label for="gift_name">Nama Hadiah</label>
                                <input type="text" class="form-control" name="gift_name" id="gift_name"
                                    placeholder="silahkan isi nama hadiah">
                            </div>
                            <div class="form-group">
                                <label for="poin">Poin</label>
                                <input type="number" class="form-control" name="poin" id="poin"
                                    placeholder="silahkan isi poin hadiah">
                            </div>
                            <div class="form-group">
                                <label for="image"> Gambar hadiah</label>
                                <input type="file" class="form-control" name="image" id="image"
                                    placeholder="silahkan isi gambar hadiah">
                            </div>
                            <div class="form-group">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" id="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</section>
<section id="daftar_hadiah" class="">
    <div class="container">
        <div class="row my-4" id="list_hadiah">

        </div>
    </div>
</section>
<section>
    <!-- Modal Edit -->
    <div class="modal fade" id="modelEdit" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="card">
                    <div class="card-body">
                        <form action="" id="formEdit">
                            <center>
                                <h4 class="card-title">Form Edit hadiah</h4>
                                <p>Silahkan isi form</p>
                            </center>
                            <br>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" id="Eid">
                                <label for="Egift_name">Nama hadiah</label>
                                <input type="text" class="form-control" name="gift_name" id="Egift_name"
                                    placeholder="silahkan isi nama hadiah">
                            </div>
                            <div class="form-group">
                                <label for="Epoin">poin hadiah</label>
                                <input type="text" class="form-control" name="poin" id="Epoin"
                                    placeholder="silahkan isi poin hadiah">
                            </div>
                            <div class="form-group">
                                <label for="image"> Gambar hadiah</label>
                                <input type="file" class="form-control" name="image" id="Eimage"
                                    placeholder="silahkan isi gambar hadiah">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" id="Esubmit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</section>

<script src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="<?= base_url('assets/js/main.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/hadiah.js'); ?>"></script>