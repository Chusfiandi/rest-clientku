<!-- Editable table -->
<div class="card ">
    <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Data Customer</h3>
    <div class="card-body">
        <div id="table" class="table-editable">
            <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success" data-toggle="modal"
                    data-target="#modelId"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
            <table class="table table-bordered table-responsive-md table-striped text-center">
                <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th class="text-center">id</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">username</th>
                        <th class="text-center">created at</th>
                        <th class="text-center">Remove</th>
                    </tr>
                </thead>
                <tbody id="list-cus">



                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="card">
                <div class="card-body">
                    <form action="" id="myform">
                        <h4 class="card-title text-center">Form tambah Customer</h4>
                        <p class="text-center">Silahkan isi form</p>
                        <br>
                        <div class="form-group">
                            <label for="name">Nama Customer</label>
                            <input type="text" class="form-control" name="name" id="name"
                                placeholder="silahkan isi nama customer..">
                        </div>
                        <div class="form-group">
                            <label for="username">username</label>
                            <input type="text" class="form-control" name="username" id="username"
                                placeholder="silahkan isi poin customer..">
                        </div>
                        <div class="form-group">
                            <label for="password"> password</label>
                            <input type="password" class="form-control" name="password" id="password"
                                placeholder="silahkan isi gambar customer..">
                        </div>
                        <div class="form-group">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" id="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Editable table -->
    <script src="<?= base_url('assets/js/jquery.validate.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="<?= base_url('assets/js/main.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/customer.js'); ?>"></script>