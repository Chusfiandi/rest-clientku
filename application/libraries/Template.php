<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template
{
    // protected $_ci;

    function templateUser($content, $data = null)
    {
        $ci = get_instance();
        $data['content'] = $ci->load->view($content, $data, TRUE);

        $ci->load->view('template/user/header');
        $ci->load->view('template/user/navigation');
        $ci->load->view('template/user/content', $data);
        $ci->load->view('template/user/footer');
    }
}
