<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('auth_model');
    }
    public function index()
    {


        if ($this->session->userdata('username')) {
            redirect('pages/product');
        }

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->load->view('template/user/header');
            $this->load->view('template/auth/login');
            // $this->load->view('template/user/footer');
        } else {
            // validasinya success
            $this->_login();
        }
    }


    private function _login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $output = $this->auth_model->getData($username, $password);
        if ($output) {
            $dataS = [
                'token' => $output['token'],
                'username' => $output['username']
            ];
            $this->session->set_userdata($dataS);
            redirect('pages/product');
        }
        redirect('auth');
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('token');

        redirect('auth');
    }


    public function blocked()
    {
        $this->load->view('auth/blocked');
    }
}