<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $ci = get_instance();
        if (!$ci->session->userdata('username')) {
            redirect('auth');
        }
        $this->load->library('template');
    }

    public function product()
    {
        $this->template->templateUser('pages/product');
    }
    public function hadiah()
    {
        $this->template->templateUser('pages/hadiah');
    }
    public function customer()
    {
        $this->template->templateUser('pages/customer');
    }
}